/*  soal 1
    buatlah variabel seperti di bawah ini */

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
 
/*  urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
    1. Tokek
    2. Komodo
    3. Cicak
    4. Ular
    5. Buaya */

var daftarHewanSorted = daftarHewan.sort();             // Jawaban Soal 1
for (let i=0; i< daftarHewanSorted.length; i++) {       // Jawaban Soal 1
    console.log(daftarHewanSorted[i]);                  // Jawaban Soal 1
}



/*  soal 2
    Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: 
    "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]" !   */

function introduce(data) {                                                                                                                                     // Jawaban Soal 2
     return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby      // Jawaban Soal 2
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 



/*  soal 3
    Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, 
    kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut. */

/*  Cara 1
function hitung_huruf_vokal(nama) {
    return nama.match(/[aiueo]/gi).length;
} */

// /*  Cara 2
const vokal = ["a", "i", "u", "e", "o"]         // Jawaban Soal 3

function hitung_huruf_vokal(nama) {             // Jawaban Soal 3    
    let hitung = 0;                             // Jawaban Soal 3
    for (let check of nama.toLowerCase()) {     // Jawaban Soal 3
        if (vokal.includes(check)) {            // Jawaban Soal 3
            hitung++;                           // Jawaban Soal 3
        }
    }
    return hitung;                              // Jawaban Soal 3
}  
// */

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2



/*  soal 4
    Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer 
    dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut. */

function hitung(nilai) {            // Jawaban Soal 4
    return ((-2) + (2*nilai));      // Jawaban Soal 4
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8

/* pattern check
   1   1   1   2
 0   1   2   3   5
-2   0   2   4   8
   2   2   2   2     */
