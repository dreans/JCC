/*  Soal 1
    Function Penghasil Tanggal Hari Esok

    Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun
    dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

    contoh 1

    var tanggal = 29
    var bulan = 2
    var tahun = 2020

    next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

    contoh 2

    var tanggal = 28
    var bulan = 2
    var tahun = 2021

    next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

    contoh 3

    var tanggal = 31
    var bulan = 12
    var tahun = 2020

    next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

    Catatan :
    1. Dilarang menggunakan new Date()
    2. Perhatikan tahun kabisat */

//  Jawaban Soal 1


var listBulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Augustus","September","Oktober","November","Desember"]

var tanggal = 29
var bulan = 2
var tahun = 2020

function next_date(tanggal , bulan , tahun ) {
    var listTanggal = []
    if (tahun%4 == 0 && bulan == 2) {
        for (let i = 1; i<=29 ; i++) {

            listTanggal.push(i)
        }
    } else if (bulan == 2){
        for (let i = 1; i<=28 ; i++) {
            listTanggal.push(i)
        }
    }else if (bulan%2 == 0 && bulan <= 7) {
        for (let i = 1; i<=30 ; i++) {
            listTanggal.push(i)
        }
    } else if (bulan%2 == 1 && bulan <= 7) {
        for (let i = 1; i<=31 ; i++) {
                listTanggal.push(i)
        }
    } else if (bulan%2 == 0 && bulan > 7) {
        for (let i = 1; i<=31 ; i++) {
            listTanggal.push(i)
        }
    } else if (bulan%2 == 1 && bulan > 7) {
        for (let i = 1; i<=30 ; i++) {
                listTanggal.push(i)
        }
    }

    if (tanggal >= listTanggal.length-1) {
        bulan += 1
        listTanggal.splice(listTanggal.length, 1, "1")
    }
    
    if ( bulan > 12) {
        tahun += 1
        bulan -= 12
    }    

    return listTanggal[tanggal] + " " + listBulan[bulan-1] + " " + tahun
}

console.log(next_date(tanggal , bulan , tahun))     // Jawaban Soal 1


/*  Soal 2.
    Function Penghitung Jumlah Kata

    Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

    Contoh

    var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
    var kalimat_2 = " Saya Iqbal"
    var kalimat_3 = " Saya Muhammad Iqbal Mubarok "


    jumlah_kata(kalimat_1) // 6
    jumlah_kata(kalimat_2) // 2
    jumlah_kata(kalimat_3) // 4

    *catatan
    Perhatikan double spasi di depan, belakang, maupun di tengah tengah kalimat */

//  Jawaban Soal 2

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(kalimat) { 
    var splitKata = kalimat.split(' ')
    var newKata = splitKata.filter(function(el) {
        return el != ''
    })
    console.log(newKata.length)
}

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4