/*  Soal 1
    buatlah variabel seperti di bawah ini */

var nilai = 90;    // Jawaban Soal 1
 
/*  pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. 
    lalu buat lah pengkondisian dengan if-elseif dengan kondisi

    nilai >= 85 indeksnya A
    nilai >= 75 dan nilai < 85 indeksnya B
    nilai >= 65 dan nilai < 75 indeksnya c
    nilai >= 55 dan nilai < 65 indeksnya D
    nilai < 55 indeksnya E  */

if (nilai >= 85) {                                          // Jawaban Soal 1
    console.log("Nilai " + nilai + " indeksnya A");         // Jawaban Soal 1
} else if (nilai >= 75 && nilai < 85) {                     // Jawaban Soal 1
    console.log("Nilai " + nilai + " indeksnya B ");        // Jawaban Soal 1
} else if (nilai >= 65 && nilai < 75) {                     // Jawaban Soal 1
    console.log("Nilai " + nilai + " indeksnya C ");        // Jawaban Soal 1
} else if (nilai >= 55 && nilai < 65) {                     // Jawaban Soal 1
    console.log("Nilai " + nilai + " indeksnya D ");        // Jawaban Soal 1
} else if(nilai < 55) {                                     // Jawaban Soal 1
    console.log("Nilai " + nilai + " indeksnya E ");        // Jawaban Soal 1
}


/*  soal 2
    buatlah variabel seperti di bawah ini */

var tanggal = 23;       // Jawaban Soal 2
var bulan = 7;          // Jawaban Soal 2
var tahun = 1995;       // Jawaban Soal 2

/*  ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan 
    buatlah switch case pada bulan, lalu muncul kan string nya dengan output 
    seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing) */

switch (bulan) {
    case 1: {console.log(tanggal + " Januari " + tahun); break;}            // Jawaban Soal 2
    case 2: {console.log(tanggal + " Februari " + tahun); break;}           // Jawaban Soal 2
    case 3: {console.log(tanggal + " Maret " + tahun); break;}              // Jawaban Soal 2
    case 4: {console.log(tanggal + " April " + tahun); break;}              // Jawaban Soal 2
    case 5: {console.log(tanggal + " Mei " + tahun); break;}                // Jawaban Soal 2
    case 6: {console.log(tanggal + " Juni " + tahun); break;}               // Jawaban Soal 2
    case 7: {console.log(tanggal + " Juli " + tahun); break;}               // Jawaban Soal 2
    case 8: {console.log(tanggal + " Agustus " + tahun); break;}            // Jawaban Soal 2
    case 9: {console.log(tanggal + " September " + tahun); break;}          // Jawaban Soal 2
    case 10: {console.log(tanggal + " Oktober " + tahun); break;}           // Jawaban Soal 2
    case 11: {console.log(tanggal + " November " + tahun); break;}          // Jawaban Soal 2
    case 12: {console.log(tanggal + " Desember " + tahun); break;}          // Jawaban Soal 2
    default: {console.log("bulan salah"); break;}                           // Jawaban Soal 2
}


/*  soal 3
    Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. 
    Looping boleh menggunakan syntax apa pun (while, for, do while).
    Output untuk n=3 :
    #
    ##
    ###
 
    Output untuk n=7 :
    #
    ##
    ###
    ####
    #####
    ######
    ####### */

var n = 7;                              // Jawaban Soal 3
for (let i = 0; i < n; i++) {           // Jawaban Soal 3
     var hasil = ""                     // Jawaban Soal 3
    for (let j = 0; j <= i; j++) {      // Jawaban Soal 3
        hasil += "#";                   // Jawaban Soal 3
    } console.log(hasil);               // Jawaban Soal 3
}

/*  soal 4
 

    berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, 
    dan berikan output sebagai berikut.
    contoh :

    Output untuk m = 3

    1 - I love programming
    2 - I love Javascript
    3 - I love VueJS
    ===

    Output untuk m = 5

    1 - I love programming
    2 - I love Javascript
    3 - I love VueJS
    ===
    4 - I love programming
    5 - I love Javascript

    Output untuk m = 7

    1 - I love programming
    2 - I love Javascript
    3 - I love VueJS
    ===
    4 - I love programming
    5 - I love Javascript
    6 - I love VueJS
    ======
    7 - I love programming


    Output untuk m = 10

    1 - I love programming
    2 - I love Javascript
    3 - I love VueJS
    ===
    4 - I love programming
    5 - I love Javascript
    6 - I love VueJS
    ======
    7 - I love programming
    8 - I love Javascript
    9 - I love VueJS
    =========
    10 - I love programming */

var m = 10;                                             // Jawaban Soal 4                               
for (i=1; i<=m; i++) {                                  // Jawaban Soal 4
    var n = '';                                         // Jawaban Soal 4
    if (i%3 == 0) {                                     // Jawaban Soal 4
        console.log(i + " - I love VueJS");             // Jawaban Soal 4
        for (j=0; j<i; j++) {                           // Jawaban Soal 4
            n += "=";                                   // Jawaban Soal 4
        } console.log(n);                               // Jawaban Soal 4
    } else if ((i+1)%3 == 0){                           // Jawaban Soal 4
        console.log(i + " - I love Javascript");        // Jawaban Soal 4
    } else {                                            // Jawaban Soal 4
        console.log(i + " - I love programming");       // Jawaban Soal 4
    }
}