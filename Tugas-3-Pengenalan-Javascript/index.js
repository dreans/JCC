/*  Soal 1
    buatlah variabel-variabel seperti di bawah ini */

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

/*  gabungkan variabel-variabel tersebut agar menghasilkan output
    saya senang belajar JAVASCRIPT */

var kata1 = pertama.substring(0,5);
var kata2 = pertama.substr(12,7);
var kata3 = kedua.substring(0,8);
var kata4 = kedua.substr(8,10).toUpperCase();
console.log(kata1.concat(kata2,kata3,kata4));   // Jawaban Soal 1


/*  Soal 2
    buatlah variabel-variabel seperti di bawah ini */

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

/*  ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
    *catatan :
    1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
    2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6) */

var angkaPertama = parseInt(kataPertama);
var angkaKedua = parseInt(kataKedua);
var angkaKetiga = parseInt(kataKetiga);
var angkaKeempat = parseInt(kataKeempat);
console.log(angkaPertama+(angkaKedua*angkaKetiga)+angkaKeempat);    // Jawaban Soal 2


/*  buatlah variabel-variabel seperti di bawah ini */

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10);       // Jawaban Soal 3 
var kataKetiga = kalimat.substring(15,18);  // Jawaban Soal 3 
var kataKeempat = kalimat.substr(19,5);     // Jawaban Soal 3 
var kataKelima = kalimat.substring(25);     // Jawaban Soal 3 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

/*  selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:
    Kata Pertama: wah
    Kata Kedua: javascript
    Kata Ketiga: itu
    Kata Keempat: keren
    Kata Kelima: sekali */