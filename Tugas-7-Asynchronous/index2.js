//  Soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

//  Jawaban Soal 2

const time = 10000

readBooksPromise(time, books[0])
    .then( (timeLeft) => {
        return readBooksPromise(timeLeft, books[1])
    })
    .then( (timeLeft) => {
        return readBooksPromise(timeLeft, books[2])
    })
    .then( (timeLeft) => {
        return readBooksPromise(timeLeft, books[3])
    })
    .catch( (timeLeft) => {
    })