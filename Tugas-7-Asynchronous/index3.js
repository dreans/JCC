//  Soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 30000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

//  async - await

const time = 10000
const start = async () => {
    try {
        const timeLeft0 = await readBooksPromise(time, books[0])
        const timeLeft1 = await readBooksPromise(timeLeft0, books[1])
        const timeLeft2 = await readBooksPromise(timeLeft1, books[2])
        const timeLeft3 = await readBooksPromise(timeLeft2, books[3])
    } catch (error) { 
    }
}

start()

